# d-apteka.ru Server Side

## Requirements

* Ruby 2.1.0

## Setting up for development

After cloning the project to the development machine:

    bundle install
    cp config/database.default.yml config/database.yml
    # Edit settings in the database.yml
    cp config/settings.local.default.yml config/settings.local.yml
    # Edit settings in the settings.local.yml
    bundle exec rake db:migrate

## Deploying to production

Capistrano is used for deployment. To deploy the app to the production server:

1. Make sure you can ssh to the server. The preferred way is by using private key access (rather than login-password).
2. In the app's dir use common Capistrano commands. To simply update the project run `cap production deploy`.

Sidekiq is started by capistrano tasks, and fails to start from time to time. So it's better to check if it's running
by logging to the server and running `ps aux | grep sidekiq`. You should see two lines in the output.

## Running tests

Tests use Rspec. To run all tests go to the app's folder and run:

    bundle exec rspec .

## Loading files

Loading of items, barcodes, item availabilities, descriptions, store codes and stores is performed by Sidekiq workers.
Workers are located at `app/workers`. Scheduling of the workers is done by whenerver gem. The schedule is located
at `config/schedule.rb`.

Dir (and method) to load files from is set in `config/settings.local.yml`.

You can see sidekiq stats at `/sidekiq` path of your site.
