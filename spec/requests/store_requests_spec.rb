require 'spec_helper'
require 'rspec_api_documentation/dsl'
require 'webmock/rspec'

resource 'Stores' do
  let!(:store1) { FactoryGirl.create :store, updated_at: 2.weeks.ago, sid: '2111590606392968' }
  let!(:store2) { FactoryGirl.create :store, updated_at: 3.months.ago }
  let!(:store3) { FactoryGirl.create :store, updated_at: 2.weeks.ago, deleted: true }
  let(:json_params) {[:sid, :name, :address, :lat, :lon, :action, :group_id]}

  get '/api/1/stores' do
    parameter :city, 'Code of the city', required: true
    parameter :page, 'Current page of stores'
    parameter :date, 'Date of the last update'

    example_request 'Getting all stores' do
      stores = JSON.parse(response_body)['response']

      expect(stores.to_json).to eq Store.where(deleted: false).to_json only: json_params
      expect(stores.count).to eq 2
      expect(status).to eq 200
    end

    example_request 'Getting updates on stores', date: 3.weeks.ago.to_s do
      stores = JSON.parse(response_body)['response']

      expect(stores.count).to eq 2
    end
  end

  get '/api/1/stores/:id/schedule' do
    before :all do
      WebMock.disable_net_connect!(allow_localhost: true)
    end

    before :each do
      stub_request(:any, /catalog.api.2gis.ru/).to_rack(FakeGisCatalog)
    end

    context 'when correct id' do

      let(:id) { store1.sid }

      example_request 'Getting store schedule' do
        days = JSON.parse(response_body)['response']

        expect(days.count).to eq 7
        expect(days[6]['intervals'][0]['from']).to eq 660 # 11:00
      end
    end
  end

end
