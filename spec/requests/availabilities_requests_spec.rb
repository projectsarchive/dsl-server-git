require 'spec_helper'
require 'rspec_api_documentation/dsl'
require 'webmock/rspec'

resource 'Items' do

  get '/api/1/items/:id/availability' do
    parameter :id, 'Id of the item to show availability of', required: true
    parameter :per_page, 'Number of items to show on page', default: 25
    parameter :page, 'Page of the result to show', default: 1
    parameter :lat, 'Latitude of the current location'
    parameter :lon, 'Longitude of the current location'

    let(:json_index_fields) { [:quantity, :price] }
    let(:json_include) { {store: {only: [:sid, :lat, :lon]}} }

    let!(:item) { FactoryGirl.create :item }

    context 'in general' do

      let!(:item1) { FactoryGirl.create :item }
      let!(:item2) { FactoryGirl.create :item }
      let!(:stores) { FactoryGirl.create_list :store, 50 }
      let!(:availabilities1) { stores.map { |store| FactoryGirl.create :item_availability, item: item1, store: store } }
      let!(:availabilities2) { stores.map { |store| FactoryGirl.create :item_availability, item: item2, store: store } }

      let(:id) { item1.id }

      example_request 'Getting availability of item in stores' do
        response = JSON.parse(response_body)

        expect(response['pagination']['count']).to eq 50
      end

    end

    context 'when negative quantity', document: false do
      let!(:store2) { FactoryGirl.create :store }
      let!(:store3) { FactoryGirl.create :store }
      let!(:availability_positive) { FactoryGirl.create :item_availability, item: item, store: store2, quantity: 3 }
      let!(:availability_negative) { FactoryGirl.create :item_availability, item: item, store: store3, quantity: -10 }

      let(:id) { item.id }

      it 'skips those positions' do
        do_request

        response = JSON.parse(response_body)
        expect(response['pagination']['count']).to eq 1
        expect(response['response'][0]['quantity']).to eq 3
      end
    end

    context 'when multiple series at one store', document: false do
      let!(:store) { FactoryGirl.create :store }
      let!(:availability1) { FactoryGirl.create :item_availability, store: store, item: item, series: '1', price: 10, quantity: 1}
      let!(:availability2) { FactoryGirl.create :item_availability, store: store, item: item, series: '2', price: 5, quantity: 3}
      let!(:availability3) { FactoryGirl.create :item_availability, store: store, item: item, series: '3', price: 6, quantity: 5}

      let(:id) { item.id }

      before(:each) { do_request ; @response = JSON.parse(response_body) }

      it('generates single line') { expect(@response['pagination']['count']).to eq 1 }
      it('sums quantities') { expect(@response['response'][0]['quantity']).to eq 9 }
      it('gets minimal price') { expect(@response['response'][0]['price']).to eq 5 }
    end

    context 'with current location' do

      let!(:store1) { FactoryGirl.create :store, lat: 57.1234, lon: 56.11 } # Distance = 1307.921897280076 km
      let!(:store2) { FactoryGirl.create :store, lat: 55.11, lon: 40.123 } # Distance = 555.367606771687 km

      let(:id) { item.id }
      let(:lat) { 50.12 }
      let(:lon) { 40.10 }

      context 'and equal prices' do
        let!(:availability1) { FactoryGirl.create :item_availability, store: store1, item: item, price: 10, quantity: 1 }
        let!(:availability2) { FactoryGirl.create :item_availability, store: store2, item: item, price: 10, quantity: 2 }

        example_request 'Getting availabilities with distance' do
          explanation 'When lat and lon params are specified the distance from that position to each store is returned.
                   Results ordered by distance first and then by price'
          response = JSON.parse(response_body)

          expect(response['response'][0]['quantity']).to eq 2
        end
      end


      context 'and different prices', document: false do
        let!(:availability1) { FactoryGirl.create :item_availability, store: store1, item: item, price: 10, quantity: 1 }
        let!(:availability2) { FactoryGirl.create :item_availability, store: store2, item: item, price: 11, quantity: 2 }

        it 'sorts by price first' do
          do_request
          response = JSON.parse(response_body)

          expect(response['response'][0]['price']).to eq 10
          expect(response['response'][0]['quantity']).to eq 1
        end
      end
    end
  end

end
