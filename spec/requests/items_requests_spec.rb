require 'spec_helper'
require 'rspec_api_documentation/dsl'
require 'webmock/rspec'

resource 'Items' do

  get '/api/1/items/search' do
    parameter :q, 'Query string to search for', required: true
    parameter :type, 'Type of the search. Possible values: name, barcode.', default: 'name'
    parameter :per_page, 'Number of items to show on page', default: 25
    parameter :page, 'Page of the result to show', default: 1

    let!(:item1) { FactoryGirl.create :item_with_barcodes, name: 'Barcode Item' }
    let!(:item2) { FactoryGirl.create :item, name: 'Unique 000127' }
    let!(:other_items) { FactoryGirl.create_list :item, 100 }

    example 'Searching for items by name' do
      explanation 'It searches with LIKE condition'

      do_request q: 'Item'

      response = JSON.parse(response_body)
      expect(response['pagination']['count']).to eq 101
    end

    example 'Searches by name in any register', document: false do
      do_request q: 'item'

      response = JSON.parse(response_body)
      expect(response['pagination']['count']).to eq 101
    end

    example 'Searching for items by barcode' do
      explanation 'It searches by exact match'

      do_request q: item1.barcodes.first.code, type: 'barcode'

      response = JSON.parse(response_body)
      expect(response['pagination']['count']).to eq 1
      expect(response['response'][0]['name']).to eq 'Barcode Item'
    end

    example 'Getting 10 items per search request' do
      do_request q: 'Item', per_page: 10

      response = JSON.parse(response_body)
      expect(response['count']).to eq 10
    end

    example 'Getting 2nd page of search results' do
      do_request q: 'Item', page: 2, per_page: 10

      response = JSON.parse(response_body)
      expect(response['response'][0]['name']).to eq Item.search_by_name('Item')[10].name
    end
  end

  get '/api/1/items/:id/description' do
    parameter :id, 'Id of the item', required: true

    let!(:item) { FactoryGirl.create :item, external_code: '000122' }
    let(:id) { item.id }

    before(:each) { Item.class_variable_set(:@@descriptions_dir, Rails.root.join('spec', 'fixtures', 'descriptions')) }
    after(:each) { Item.class_variable_set(:@@descriptions_dir, nil) }

    example 'Getting description of an item' do
      explanation 'It returns the description in HTML format. If there\'s no description for the item, an empty string is returned.'

      do_request

      response = JSON.parse(response_body)
      expect(response['response']).to eq 'Description of 000122'
    end

    context 'when error getting RLS code', document: false do
      before(:each) { stub_request(:any, /www.rlsnet.ru/).to_rack(FakeRlsInformer) }
      let!(:item_wrong_rls) { FactoryGirl.create :item, rls_code: '11111' }
      let(:id) { item_wrong_rls.id }

      it 'returns message' do
        do_request

        response = JSON.parse(response_body)
        expect(response['response']).to include 'Не удается получить описание'
      end
    end
  end
end
