require 'sinatra/base'

class FakeGisCatalog < Sinatra::Base
  get '/profile' do
    json_response 200, 'gis_catalog_correct.json'
  end

  get '/searchinrubric' do
    json_response 200, 'gis_rubrics.json'
  end

  private

  def json_response(response_code, file_name)
    content_type :json
    status response_code
    File.open(File.dirname(__FILE__) + '/json_fixtures/' + file_name, 'rb').read
  end
end