require 'sinatra/base'

class FakeRlsInformer < Sinatra::Base
  post '/export_prep_id_6084.htm' do
    if params[:login]=='testalvik' && params[:password]=='testpassword'
      respond 200, correct_description
    else
      respond 200, wrong_login_description
    end
  end

  post '/export_prep_id_9999.htm' do
    respond 200, non_utf8_description
  end

  post '/export_prep_id_*.htm' do
    respond 200, code_not_found_description
  end

  private

  def respond(code, body)
    status code
    body
  end

  def correct_description
    '<div class="rls_export">Rls Description</div>'
  end

  def code_not_found_description
    'Not found prep'
  end

  def wrong_login_description
    '<p class="error">������ �����������.</p>'
  end

  def non_utf8_description
    '<div class="rls_export">Тестовое описание</div>'.b
  end
end