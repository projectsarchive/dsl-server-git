FactoryGirl.define do
  factory :barcode do
    code { rand(999999999).to_s }
    item
  end

  factory :item do
    external_code { rand(999999999).to_s }
    sequence(:name) { |n| "Test Item #{n}" }

    factory :item_with_barcodes do
      ignore do
        barcodes_count 1
      end

      after(:create) do |item, evaluator|
        create_list(:barcode, evaluator.barcodes_count, item: item)
      end
    end
  end

  factory :store do
    external_code '1'
    sequence(:name) { |n| "Test Store #{n}" }
    deleted false
    group_id { rand(1000000000..9999999999) }
    sid { rand(1000000000..9999999999) }
    lat { rand(-90.000000000...90.000000000) }
    lon { rand(-180.000000000...180.000000000) }
    sequence(:address) { |n| "Street Name, #{n}" }
  end

  factory :item_availability do
    item { create(:item) }
    store { create(:store) }
    quantity { rand(1..10000) }
    price { rand(10..10000) }
  end
end
