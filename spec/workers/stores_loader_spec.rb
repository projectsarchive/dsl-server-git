# encoding: UTF-8
require 'spec_helper'

require 'sidekiq/testing'
Sidekiq::Testing.inline!

describe StoresLoader do
  before :each do
    stub_request(:any, /catalog.api.2gis.ru/).to_rack(FakeGisCatalog)
  end
  describe '.load_stores_from_2gis' do
    it 'loads stores' do
      StoresLoader.new.load_stores_from_2gis 1
    end
  end
end