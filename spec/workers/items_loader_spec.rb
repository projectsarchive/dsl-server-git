# encoding: UTF-8
require 'spec_helper'

require 'sidekiq/testing'
Sidekiq::Testing.inline!

describe ItemsLoader do

  let(:loader) { ItemsLoader.new }
  let(:filename) { Rails.root.join('spec', 'fixtures', 'items.csv') }

  describe '.load_items' do
    before :each do
      loader.perform filename
      @item = Item.find_by_external_code(external_code)
    end

    let(:external_code) { '00012655' }
    let(:item_name) { 'AVENT Бутылочка д/кормления 125,0 №2 /86040/' }

    it 'creates all items' do
      expect(Item.all.count).to eq 7
    end

    it 'loads correct names' do
      expect(@item.name).to eq item_name
    end

    it 'removes unprintable characters from names' do
      item2 = Item.find_by_external_code '00011131'

      expect(item2.name).to eq 'NUK Бутылочка стекл 230мл+сос с вент лат р1/10745018/'
    end

    it 'updates existing items' do
      Item.find_by_external_code(external_code).update(name: 'Test')
      loader.perform filename

      item = Item.find(@item.id)

      expect(item.name).to eq item_name
      expect(Item.where(external_code: external_code).count).to eq 1
    end
  end
end
