# encoding: UTF-8
require 'spec_helper'

require 'sidekiq/testing'
Sidekiq::Testing.inline!

describe RlsCodesLoader do

  let(:loader) { RlsCodesLoader.new }
  let(:filename_items_descriptions) { Rails.root.join('spec', 'fixtures', 'descriptions.csv') }

  let!(:item) { FactoryGirl.create :item, external_code: '00013151' }
  let!(:item2) { FactoryGirl.create :item, external_code: '00013156', rls_code: '121' }

  before :each do
    loader.perform filename_items_descriptions
    item.reload
    item2.reload
  end

  it 'adds rls codes to items' do
    expect(item.rls_code).to eq '326477'
  end

  it 'rewrites rls codes' do
    expect(item2.rls_code).to eq '324269'
  end

  context 'when item not found' do
    it 'does not create new item' do
      expect(Item.count).to eq 2
    end
    it 'leaves warning in the log' do
      Rails.logger.should_receive(:warn)
      loader.perform filename_items_descriptions
    end
  end
end
