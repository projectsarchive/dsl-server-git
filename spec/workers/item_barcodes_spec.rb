# encoding: UTF-8
require 'spec_helper'

require 'sidekiq/testing'
Sidekiq::Testing.inline!

describe ItemsLoader do

  let(:loader) { ItemBarcodesLoader.new }
  let(:filename_items_barcodes) { Rails.root.join('spec', 'fixtures', 'barcodes.csv') }

  describe '.load_items_barcodes' do
    before :each do
      loader.perform filename_items_barcodes
      @item = Item.find_by_external_code(external_code)
    end

    let(:external_code) { '00002922' }
    let(:item_name) { 'AVENT Бутылочка д/кормления 260,0 №2 /8059/86060/' }
    let(:barcode) { '5012909007825' }

    it 'creates items' do
      expect(@item.name).to eq item_name
    end

    it 'reuses existing items' do
      Item.find_by_external_code(external_code).update(name: 'Test')
      loader.perform filename_items_barcodes

      item = Item.find(@item.id)

      expect(item.name).to eq 'Test'
      expect(Item.where(external_code: external_code).count).to eq 1
    end

    it 'loads all barcodes for an item' do
      expect(Barcode.where(item_id: @item.id).count).to eq 3
      assert_not_nil Barcode.find_by_code barcode
    end
  end
end
