# encoding: UTF-8
require 'spec_helper'

require 'sidekiq/testing'
Sidekiq::Testing.inline!
#Sidekiq::Logging.logger = nil

describe ItemAvailabilityLoader do

  let(:loader) { ItemAvailabilityLoader.new }
  let(:filename_items_availability) { Rails.root.join('spec', 'fixtures', 'nal.csv') }
  let!(:item1) { FactoryGirl.create(:item, external_code: '00012655') }
  let!(:item2) { FactoryGirl.create(:item, external_code: '00004402') }
  let!(:store1) { FactoryGirl.create(:store, external_code: '8') }
  let!(:store2) { FactoryGirl.create(:store, external_code: '10') }

  it 'loads quantity and prices' do
    loader.perform filename_items_availability
    item_availability = ItemAvailability.where(store: store2, item: item1).first

    assert_not_nil item_availability
    expect(item_availability.price).to eq 158
    expect(item_availability.quantity).to eq 2
  end

  it 'loads one quantity for duplicate lines' do
    loader.perform filename_items_availability
    availabilities = ItemAvailability.where(store: store1, item: item2)
    expect(availabilities.count).to eq 1
    expect(availabilities.first.quantity).to eq 3.0
  end

  it 'removes availabilities of items not in file' do
    new_item = FactoryGirl.create(:item, external_code: '1111')
    FactoryGirl.create(:item_availability, item: new_item, store: store1)
    # Old unchanged files detected by updated_at field value, so let the prev record get "old"
    sleep 1
    loader.perform filename_items_availability

    expect(ItemAvailability.where(store: store1, item: new_item).count).to eq 0
  end

  it 'does not clear availability of items in stores not in file' do
    other_store = FactoryGirl.create(:store, external_code: '333')
    FactoryGirl.create(:item_availability, item: item1, store: other_store)
    loader.perform filename_items_availability

    expect(ItemAvailability.where(store: other_store).count).to eq 1
  end

  it 'loads all the series of a single item' do
    store3 = FactoryGirl.create(:store, external_code: '9')
    loader.perform filename_items_availability

    item2_availabilities = ItemAvailability.where(store: store3, item: item2)

    expect(item2_availabilities.count).to eq 2
    expect(item2_availabilities.first.series).to eq '10803975'
    expect(item2_availabilities.first.price).to eq 151.0
    expect(item2_availabilities.last.series).to eq '10803976'
    expect(item2_availabilities.last.price).to eq 156.2
  end

  context 'when code is not specified' do
    let(:filename_empty_code) { Rails.root.join('spec', 'fixtures', 'nal_empty_code.csv') }

    it 'continues loading' do
      expect do
        loader.perform filename_empty_code
      end.to_not raise_error
    end

    it 'skips the item' do
      loader.perform filename_empty_code

      expect(ItemAvailability.count).to eq 3
    end
  end
end
