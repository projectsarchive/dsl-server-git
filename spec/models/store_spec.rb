require 'spec_helper'

describe Store do
  describe '.updated_after date' do
    let!(:store1) { FactoryGirl.create :store, name: 'Store 1', updated_at: 1.week.ago }
    let!(:store2) { FactoryGirl.create :store, updated_at: 3.months.ago }
    let!(:store3) { FactoryGirl.create :store, updated_at: 3.months.ago, deleted: true }

    context 'when date is specified' do
      context 'and correct' do
        it 'returns models updated after the date' do
          stores = Store.updated_after 2.week.ago.to_s

          expect(stores.count).to eq 1
          expect(stores.first.name).to eq 'Store 1'
        end
      end
      context 'and incorrect' do
        it 'throws an error' do
          expect{Store.updated_after 'Date that cannot be parsed'}.to raise_exception Exception
        end
      end
    end

    context 'when date is not specified' do
      it 'returns all non-deleted stores' do
        stores = Store.updated_after nil
        expect(stores.count).to eq 2
        expect(stores.last.deleted).to be_falsey
      end
    end
  end
end
