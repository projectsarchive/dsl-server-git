require 'spec_helper'

describe Item do
  describe '.description' do
    before(:all) { Item.set_descriptions_dir(Rails.root.join('spec', 'fixtures', 'descriptions')) }

    context 'when file description present' do
      it 'returns description from file' do
        item = Item.new(external_code: '000122')
        expect(item.description).to eq 'Description of 000122'
      end

      context 'and has rls code' do
        it 'returns description from file' do
          item = Item.new(external_code: '000122', rls_code: '15717')
          expect(item.description).to eq 'Description of 000122'
        end
      end
    end

    context 'when file description absent' do
      context 'and no rls code' do
        it 'returns empty string' do
          item = Item.new(external_code: '111111')
          expect(item.description).to eq ''
        end
      end
      context 'and has rls code' do
        it 'returns description from rls' do
          item = Item.new(external_code: '111111', rls_code: '15717')
          allow(item).to receive(:rls_description).with(item.rls_code) {'Rls Description of 000122'}

          expect(item.description).to eq 'Rls Description of 000122'
        end
      end
    end
  end

  describe '.rls_description' do
    let(:rls_description) {double('rls_description')}
    subject {get_subject}

    before(:each) { Item.class_variable_set(:@@rls_reader, rls_description) }
    after(:each) { Item.class_variable_set(:@@rls_reader, nil) }

    it 'returns RLS description' do
      stub_rls_with {'Rls Description'}
      should eq 'Rls Description'
    end

    context 'when error receiving description' do
      before(:each) {stub_rls_with { throw Exception.new('Error description') }}
      it { should include 'Error description' }
      it 'logs the error' do
        Rails.logger.should_receive(:error)
        get_subject
      end
    end

    def stub_rls_with
      allow(rls_description).to receive(:get_by_code).with('123') { yield }
    end

    def get_subject
      Item.new.rls_description('123')
    end
  end
end
