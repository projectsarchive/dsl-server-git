require 'spec_helper'

describe RlsDescription do
  #subject(:desc) { RlsDescription.new({username: 'allweek', password: 'vmHe54bt'}) }
  subject(:desc) { RlsDescription.new({username: 'testalvik', password: 'testpassword'}) }
  describe '.get_by_code' do
    before :each do
      #WebMock.allow_net_connect!
      stub_request(:any, /www.rlsnet.ru/).to_rack(FakeRlsInformer)
    end

    it 'returns HTML description' do
      response = desc.get_by_code '6084'
      expect(response).to include('rls_export')
    end

    context 'when description encoding is non-utf' do
      it 'returns it in UTF-8' do
        response = desc.get_by_code '9999'
        expect(response.encoding).to eq Encoding::UTF_8
        expect(response).to include('Тестовое описание')
      end
    end

    context 'with incorrect code' do
      it 'raises exception' do
        expect { desc.get_by_code '123' }.to raise_error
      end
    end

    context 'with incorrect username and password' do
      it 'throws an exception' do
        incorrect_desc = RlsDescription.new({username: 'test', password: 'test'})
        expect { incorrect_desc.get_by_code '6084' }.to raise_error
      end
    end
  end
end