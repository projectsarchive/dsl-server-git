class Api::V1::ItemAvailabilitiesController < ApiBaseController
  version 1
  caches :index, :caches_for => 5.minutes

  def index
    item = Item.find params[:item_id]
    item_availabilities = item.item_availabilities

    if params['lat'] && params['lon']
      item_availabilities = item_availabilities.by_store_distance([params['lat'], params['lon']])
    end

    expose item_availabilities.by_store.page(params['page']).per(params['per_page']),
           only: [:quantity, :price, :distance], include: [store: {only: [:sid, :lat, :lon]}]
  end
end