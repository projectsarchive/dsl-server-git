class Api::V1::ItemsController < ApiBaseController
  version 1
  caches :index, :show, :caches_for => 5.minutes

  def search
    type = params['type'] || 'name'
    unless %w(name barcode).include? type
      error! :bad_request, "Type param should be name' or 'barcode'. '#{params['type']}' given"
    end

    result = Item.page(params['page']).per(params['per_page'])
    result = result.search_by_name params['q'] if type == 'name'
    result = result.includes(:barcodes).where('barcodes.code' => params['q']).references(:barcodes) if type == 'barcode'

    expose result, only: [:id, :name]
  end

  def description
    @item = Item.find(params[:id])
    expose @item.description
  end
end
