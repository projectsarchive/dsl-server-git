class Api::V1::StoresController < ApiBaseController
  version 1
  caches :index, :show, :caches_for => 30.minutes

  def index
    stores = Store.updated_after(params[:date]).page(params['page']).per(params['per_page'])
    expose stores, only: [:sid, :name, :address, :lat, :lon, :action, :group_id]
  end

  def schedule
    store = Store.find_by_sid! params[:id]

    expose store.schedule
  end
end