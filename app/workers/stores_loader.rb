require 'open-uri'
require 'json'

class StoresLoader
  include Sidekiq::Worker
  sidekiq_options queue: 'default'

  def perform(city_id = nil)
    load_stores_from_2gis(city_id)
  end



  def load_stores_from_2gis(city_id = nil)
    return {error: 'No GIS API Key'} if Settings.GIS_API_KEY.blank?

    city_id = city_id || 1
    city_name = get_city_name_by_id(city_id)

    page_size = 50
    uri = "http://catalog.api.2gis.ru/searchinrubric?what=%D0%90%D0%BF%D1%82%D0%B5%D0%BA%D0%B8&where=#{city_name}&rubrics=&version=1.3&key=#{Settings.GIS_API_KEY}&output=json&pagesize=#{page_size}&page="

    i = pages = 1
    stats = {total: 0, added: 0, updated: 0, removed: 0, unchanged: 0}
    current_stores_ids = []
    begin
      data = JSON.parse(open(uri + i.to_s).read)
      return {error: 'No data returned'} unless data
      if i == 1
        stats[:total] = data['total'].to_f
        pages = (stats[:total] / page_size.to_f).ceil
      end
      data['result'].each do |store|
        next unless store['lat'] && store['lon']
        code = update_insert_store store, city_id
        stats[code] += 1
        current_stores_ids << store['id']
      end
      i += 1
    end while i <= pages

    stats[:removed] = Store.where(city: city_id, deleted: false).where.not(sid: current_stores_ids).update_all(deleted: true)

    stats
  end

  def update_insert_store(store_info, city_id)
    store_info['group_id'] = store_info['firm_group']['id'] || ''
    store = Store.where(sid: store_info['id'], city: city_id, deleted: false).first || Store.new
    code = store.new_record? ? :added : :unchanged
    attributes_to_update = {deleted: false, sid: store_info['id'], city: city_id}
    store.attributes.each do |key, value|
      next if key == 'id' || !store_info[key]
      if value.to_s != store_info[key].to_s
        attributes_to_update[key.to_sym] = store_info[key]
        code = :updated if code != :added
      end
    end
    store.update_attributes!(attributes_to_update) if code != :unchanged

    code
  end

  def get_city_name_by_id(id)
    case id
      when 1
        'chelyabinsk'
      else
        'moscow'
    end
  end
end