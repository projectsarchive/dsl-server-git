class StoresCodesLoader < Support::InfoLoader
  def perform(filename=nil)
    filename = filename || 'ua_codes.csv'

    header = %w(sid external_code)

    read_file filename, header do |row|
      store = Store.find_by_sid row['sid']
      if store
        store.external_code = row['external_code']
        store.save!
      end
    end
  end
end
