require 'net/ftp'

class Support::FileLoader
  def read(filename)
    if @file
      close unless @file.closed?
      @file = nil
      @temp = nil
    end
    if File.exist? filename
      @file = File.new(filename)
      @temp = false
    else
      begin
        if Settings.FileLoader.Method == :file
          @temp = false
          if Settings.FileLoader.Options.LocalDir
            @file = File.new(File.join(Settings.FileLoader.Options.LocalDir, filename))
          else
            @file = File.new(File.join('upload', filename))
          end
        elsif Settings.FileLoader.Method == :ftp # TODO Test FTP
          @temp = true
          options = Settings.FileLoader.Options
          ftp = Net::FTP.open(options.FtpHost, options.FtpLogin, options.FtpPassword)
          ftp.chdir(options.FtpDir) unless options.FtpDir.blank?
          @file = Tempfile.new(filename)
          ftp.get(filename, @file.path)
        end
      rescue Exception => e
        Rails.logger.warn("Unable to load file #{filename}. Error: #{e.message}")
        @file = nil
      end
    end
    @file
  end

  def close
    return unless @file
    @file.close unless @file.closed?
    @file.unlink if @temp
    @file = nil
    @temp = nil
  end
end
