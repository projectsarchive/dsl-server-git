class Support::InfoLoader
  include Sidekiq::Worker
  sidekiq_options queue: 'default'
  sidekiq_options retry: false

  def file_loader
    @file_loader ||= Support::FileLoader.new
  end

  def read_file filename, header
    file = file_loader.read(filename)
    return unless file
    CsvReader::read_spreadsheet File.absolute_path(file), header do |row, row_number|
      yield row, row_number
    end
    file_loader.close
  end
end
