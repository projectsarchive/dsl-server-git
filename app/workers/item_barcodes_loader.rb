class ItemBarcodesLoader < Support::InfoLoader
  # Loads items and barcodes from a file
  # @param [String] filename [optional] Full name of the file. Default is <rails_root>/upload/barcodes.csv
  # @return [void] Returns nothing
  def perform(filename=nil)
    filename = filename || 'barcodes.csv'

    header = %w(external_code code name)

    read_file filename, header do |row|
      item = Item.find_by external_code: row['external_code']
      unless item
        item = Item.new
        item.name = row['name']
        item.external_code = row['external_code']
        item.save!
      end

      barcode = Barcode.find_or_initialize_by item_id: row['item_id']
      barcode.item_id = item.id
      barcode.code = row['code']
      barcode.save!
    end

  end
end