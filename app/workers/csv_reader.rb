require 'csv'

class CsvReader
  def self.read_spreadsheet(named, with_fields)

    counter = 0
    IO.foreach(named.to_s,{encoding: 'BOM|UTF-8'}) do |line|
      #quote_chars = %w(" | ~ ^ & *)
      #begin
      #  quote_char = quote_chars.shift
      #  spread = CSV.parse(line, col_sep: ';', quote_char: quote_char)
      #rescue CSV::MalformedCSVError
      #  quote_chars.empty? ? raise : retry
      #end
      counter += 1
      begin
        CSV.parse(line, encoding: Encoding::UTF_8, col_sep: ';', quote_char: '"') do |spreadsheet_row|
          spreadsheet_row.map! { |item| item ? item.strip : '' }
          row = Hash[[with_fields, spreadsheet_row].transpose]
          yield row, counter
        end
      rescue CSV::MalformedCSVError
        raise CSV::MalformedCSVError.new("Malformed CSV at line #{counter}")
      rescue Exception => e
        raise e.exception("File line #{counter}: #{e.to_s}")
      end
    end

    #options = { col_sep: ";", quote_char:'"', encoding: Encoding::UTF_8 }
    ##CSV.foreach(named.to_s, options) do |i|
    #File.foreach(named.to_s, mode: 'r:utf-8') do |csv_line|
    #  csv_line.gsub!('\"', '""')
    #  begin
    #    i = CSV.parse(csv_line, options).first
    #  rescue Exception => e
    #    raise "Error reading #{named.to_s} on line #{$.}:\n#{e}"
    #  end
    #  spreadsheet_row = i.map { |item| item.strip }
    #  row = Hash[[with_fields, spreadsheet_row].transpose]
    #  yield row
    #end

  end
end