class ItemAvailabilityLoader < Support::InfoLoader
  # Loads items availability in stores from a file
  # @param [String] filename [optional] Name of the file to load from. Default is /upload/nal.csv
  def perform(filename=nil)
    filename = filename || 'nal.csv'

    header = %w(store_external_id item_name item_code price quantity producer)

    stores_in_file = Set.new

    stores_not_found = Set.new
    items_not_found = Set.new
    current_date = Time.now
    read_file filename, header do |row, row_number|
      data = Hash.new

      if row['item_code'].blank?
        logger.warn "No external code specified for item at line #{row_number}"
        next
      end

      next if stores_not_found.include?(row['store_external_id'])
      data['store'] = Store.find_by_external_code row['store_external_id']
      unless data['store']
        logger.error "Store with code '#{row['store_external_id']}' not found"
        stores_not_found.add row['store_external_id']
        next
      end

      stores_in_file << data['store'].id

      item_external_code = get_item_external_code row['item_code']
      next if items_not_found.include?(item_external_code)
      data['item'] = Item.find_by_external_code item_external_code
      unless data['item']
        logger.error "Item with code '#{item_external_code}' not found"
        items_not_found.add item_external_code
        next
      end
      data['quantity'] = row['quantity']
      data['price'] = row['price']
      data['series'] = get_item_series row['item_code']

      availability = ItemAvailability.where(store: data['store'], item: data['item'], series: data['series']).first_or_initialize
      updated = availability.update_attributes data.to_hash
      logger.error "Unable to update availability: #{availability.inspect}. Errors: #{availability.errors.full_message}" unless updated
    end
    ItemAvailability.delete_all(['store_id IN (?) AND updated_at < ?', stores_in_file, current_date])
  end

  def get_item_external_code(item_code)
    item_code[2, 8]
  end

  def get_item_series(item_code)
    item_code[10,8]
  end
end