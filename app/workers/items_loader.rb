class ItemsLoader < Support::InfoLoader
  def perform(filename=nil)
    filename = filename || 'items.csv'

    header = %w(external_code name)

    read_file filename, header do |row|
      item = Item.find_or_initialize_by external_code: row['external_code']
      row['name'].gsub!(/[^[:print:]]/,'')
      item.attributes = row.to_hash
      item.save!
    end
  end
end
