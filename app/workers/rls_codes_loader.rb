class RlsCodesLoader < Support::InfoLoader
  def perform(filename=nil)
    filename = filename || 'descriptions.csv'

    header = %w(external_code rls_code)

    read_file filename, header do |row|
      item = Item.find_by external_code: row['external_code']
      if item
        item.rls_code = row['rls_code']
        item.save!
      else
        Rails.logger.warn "Unable to find item with external code #{row['external_code']}"
      end
    end
  end
end
