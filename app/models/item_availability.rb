class ItemAvailability < ActiveRecord::Base
  belongs_to :item
  belongs_to :store

  acts_as_mappable :through => :store

  scope :by_store, -> {select('store_id, min(price) as price, sum(quantity) as quantity').
      where('quantity > 0').group(:store_id).order('price ASC')}

  def self.by_store_distance(current_location)
    distance_qurey = ItemAvailability.distance_sql(Geokit::LatLng.normalize(current_location))
    select("max(#{distance_qurey}) distance").joins(:store).order('distance asc')
  end
end
