require 'net/http'

class Store < ActiveRecord::Base
  include RocketPants::Cacheable

  has_many :item_availabilities

  acts_as_mappable :default_units => :kms,
                   :default_formula => :sphere,
                   :lat_column_name => :lat,
                   :lng_column_name => :lon

  def self.updated_after date=nil
    if date
      begin
        date = Time.parse date if date.is_a? String
      rescue
        raise Exception.new('Unknown date format')
      end
      where('updated_at > ?', date.to_s)
    else
      where deleted: false
    end
  end

  def schedule
    uri = URI("http://catalog.api.2gis.ru/profile?version=1.3&key=#{Settings.GIS_API_KEY}&id=#{sid}")

    response = JSON.load(Net::HTTP.get(uri))

    result = Array.new
    response['schedule'].each do |day_name, schedule|
      day_index = Date::ABBR_DAYNAMES.index(day_name)
      day_index = 7 if day_index == 0
      intervals = Array.new
      schedule.each do |key, value|
        from = Time.parse(value['from']).seconds_since_midnight.to_i / 60
        to = Time.parse(value['to']).seconds_since_midnight.to_i / 60
        intervals << { from: from, to: to }
      end
      result << { day: day_index, intervals: intervals }
    end
    result
  end

  # Virtual attributes

  def as_json(options = { })
    # just in case someone says as_json(nil) and bypasses
    # our default...
    super((options || { }).merge({
                                     :methods => [:action]
                                 }))
  end

  def action
    deleted ? 'delete' : 'update'
  end
end
