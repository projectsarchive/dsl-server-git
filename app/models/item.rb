class Item < ActiveRecord::Base
  include RocketPants::Cacheable

  has_many :barcodes, dependent: :destroy
  has_many :item_availabilities, dependent: :destroy

  @@descriptions_dir ||= Rails.root.join('upload', 'descriptions')
  @@rls_reader ||= RlsDescription.new({username: Settings.RLS_USERNAME, password: Settings.RLS_PASSWORD})

  def self::search_by_name(q)
    # Using the Squeel gem
    where{ name =~ "%#{q}%" }
  end

  def self.set_descriptions_dir new_dir
    @@descriptions_dir = new_dir
  end

  def description
    file_description = @@descriptions_dir.join("#{external_code}.html")
    if file_description.exist?
      File.read file_description
    elsif rls_code
      rls_description rls_code
    else
      ''
    end
  end

  def rls_description(code)
    begin
      @@rls_reader.get_by_code code
    rescue Exception => e
      Rails.logger.error("Item ##{id} RLS code #{code}: #{e.message}")
      "Не удается получить описание. Попробуйте позже или обратитесь к разработчикам. Описание ошибки: #{e.message}."
    end
  end
end
