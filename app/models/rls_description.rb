class RlsDescription
  attr_accessor :username
  attr_accessor :password
  attr_accessor :host

  def initialize(h = Hash.new)
    h.each {|k,v| send("#{k}=",v)}
    @host ||= 'www.rlsnet.ru'
  end

  def get_by_code(code)
    query = {login: username, password: @password}.to_query
    url = URI.parse("http://#{@host}/export_prep_id_#{code}.htm")
    request = Net::HTTP::Post.new(url.path)
    request.content_type = 'application/x-www-form-urlencoded'
    request.body = query
    response = Net::HTTP.start(url.host, url.port) { |http| http.request(request) }
    result = response.body

    if result.include?('class="error"') || !result.include?('rls_export')
      unless result.encoding == Encoding::UTF_8
        # It's duplicated with the code below because with `encode` method called first it's removes all the text from
        # the standart output. If we put force_encoding to the front, RubyMine hangs when trying to evaluate it
        # in case of an error
        result = result.encode('UTF-8', :invalid => :replace, :undef => :replace, :replace => '').force_encoding('UTF-8')
      end
      message = Nokogiri::HTML(result)
      message.css('script').remove
      message.css('noindex').remove
      message.css('.noprint').remove
      message = ActionView::Base.full_sanitizer.sanitize(message.xpath('//body').first.to_html)
      message = message.gsub(/\r/,'').gsub(/\t/,'').gsub(/\n/,'').gsub(/\s+/,' ')
      raise Exception.new "An error occurred while getting RLS Description. #{message}"
    end

    unless result.encoding == Encoding::UTF_8
      result = result.force_encoding('UTF-8')
    end

    result
  end
end
