ActiveAdmin.register Item do
  filter :id
  filter :name
  filter :active
  filter :external_code

  # See http://stackoverflow.com/questions/7754896/import-csv-data-in-a-rails-app-with-activeadmin
  collection_action :import_items do
    ItemsLoader.perform_async
    redirect_to({action: :index}, {notice: 'Items loading scheduled!'})
  end

  collection_action :import_rls_codes do
    RlsCodesLoader.perform_async
    redirect_to({action: :index}, {notice: 'Rls Codes loading scheduled!'})
  end

  action_item only: :index do
    link_to 'Import items', action: :import_items
  end

  action_item only: :index do
    link_to 'Import RLS codes', action: :import_rls_codes
  end
end
