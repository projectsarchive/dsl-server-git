ActiveAdmin.register Store do
  filter :group_id
  #filter :name, as: :check_boxes, collection: proc { Store.uniq.pluck(:name).sort }, label: 'Company'
  filter :name, label: 'Company'

  index do
    column :external_code, sortable: :external_code do |external_code|
      best_in_place external_code, :external_code, type: :input, path: [:admin, external_code]
    end
    column :name
    column :group_id
    column :address
    column :created_at
    column :updated_at
    actions
  end

  # See http://stackoverflow.com/questions/7754896/import-csv-data-in-a-rails-app-with-activeadmin
  collection_action :import_2gis do
    StoresLoader.perform_async
    redirect_to({action: :index}, {notice: 'Stores update scheduled'})
  end

  collection_action :import_ua_codes do
    StoresCodesLoader.perform_async
    redirect_to({action: :index}, {notice: 'Stores codes import scheduled'})
  end

  action_item :only => :index do
    link_to 'Import from 2GIS', action: :import_2gis
  end

  action_item :only => :index do
    link_to 'Import UA codes', action: :import_ua_codes
  end

  controller do
    def permitted_params
      params.permit(:store => [:external_code])
    end
  end
end
