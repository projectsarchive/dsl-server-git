ActiveAdmin.register ItemAvailability do
  filter :store
  filter :item_id, label: 'Item id'

  collection_action :import_availability do
    ItemAvailabilityLoader.perform_async
    redirect_to({action: :index}, {notice: 'Items availability loading started!'})
  end

  collection_action :import_availability_direct do
    ItemAvailabilityLoader.new.perform
    redirect_to({action: :index}, {notice: 'Items availability loaded!'})
  end

  action_item :only => :index do
    link_to 'Import availability', action: :import_availability
  end

  action_item :only => :index do
    link_to 'Direct import', action: :import_availability_direct
  end

end
