ActiveAdmin.register Barcode do
  filter :item_id, label: 'Item id'
  
  collection_action :import_barcodes do
    ItemBarcodesLoader.perform_async
    redirect_to({action: :index}, {notice: 'Items barcodes loading started!'})
  end

  action_item :only => :index do
    link_to 'Import barcodes', action: :import_barcodes
  end

end
