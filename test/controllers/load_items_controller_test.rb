require 'test_helper'

class LoadItemsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get descriptions" do
    get :descriptions
    assert_response :success
  end

end
