class AddExternalCodeToStores < ActiveRecord::Migration
  def change
    add_column :stores, :external_code, :string, index: true
  end
end
