class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name, index: true, COLLATE: :NOCASE
      t.string :description_file
      t.string :rls_code, index: true
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
