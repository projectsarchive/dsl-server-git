class AddExternalCodeToItems < ActiveRecord::Migration
  def change
    add_column :items, :external_code, :string
    add_index :items, :external_code
  end
end
