class CreateItemAvailabilities < ActiveRecord::Migration
  def change
    create_table :item_availabilities do |t|
      t.references :item, index: true
      t.references :store, index: true
      t.float :quantity
      t.float :price

      t.timestamps
    end
  end
end
