class AddSeriesToItemAvailability < ActiveRecord::Migration
  def change
    add_column :item_availabilities, :series, :string, index: true
  end
end
