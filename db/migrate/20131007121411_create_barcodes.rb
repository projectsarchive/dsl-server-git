class CreateBarcodes < ActiveRecord::Migration
  def change
    create_table :barcodes do |t|
      t.references :item, index: true
      t.string :code, index: true

      t.timestamps
    end
  end
end
