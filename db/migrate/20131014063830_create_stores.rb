class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :Store
      t.string :name, index: true, COLLATE: :NOCASE
      t.float :lat
      t.float :lon
      t.text :address, COLLATE: :NOCASE
      t.string :sid, index: true
      t.string :group_id, index: true
      t.integer :city, index: true
      t.integer :data
      t.boolean :deleted

      t.timestamps
    end
  end
end
