source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', git: 'https://github.com/rails/rails'
gem 'rails', '~> 4.0.0'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

gem 'sidekiq'
gem 'sinatra', '>= 1.3.0', :require => nil
#gem 'sidekiq_status', git: 'https://github.com/cryo28/sidekiq_status', branch: 'master'

gem 'rails_config'

gem 'activeadmin', git: 'https://github.com/gregbell/active_admin'
gem 'best_in_place', git: 'https://github.com/bernat/best_in_place'
gem 'devise'

gem 'mysql2'
gem 'squeel', '~> 1.2.1'
gem 'nokogiri'

gem 'whenever', :require => false
gem 'newrelic_rpm'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :development, :test do
  gem 'rspec-rails', '>= 2.0'
  gem 'factory_girl_rails', '>= 4.1.0'
  # Use sqlite3 as the database for Active Record
  gem 'sqlite3'
  gem 'rspec_api_documentation'
end

group :test do
  gem 'webmock'
  gem 'test-unit'
  gem 'minitest'
end

group :production do
  gem 'rails_12factor'
end

# Use unicorn as the app server
gem 'unicorn'
group :development do
  gem 'capistrano-rails',   '~> 1.1', require: false
  gem 'capistrano-bundler', '~> 1.1', require: false
  gem "capistrano-sidekiq"
  gem 'capistrano', '~> 3.1.0', require: false
  gem 'capistrano-rvm',   '~> 0.1', require: false
  gem 'capistrano-rbenv', '~> 2.0', require: false
end

# API gems
gem 'rocket_pants', '~> 1.0'
gem 'kaminari'
gem 'raddocs'
#gem 'grape'

gem 'geokit-rails'

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# Use debugger
# gem 'debugger', group: [:development, :test]
