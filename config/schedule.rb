# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

set :output, "#{path}/log/cron_log.log"

every 12.hours do
  runner 'ItemsLoader.perform_async'
  runner 'ItemBarcodesLoader.perform_async'
  runner 'ItemAvailabilityLoader.perform_async'
  runner 'RlsCodesLoader.perform_async'
end

every 1.week do
  runner 'StoresLoader'
  runner 'StoresCodesLoader'
end

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
