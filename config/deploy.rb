# -*- encoding : utf-8 -*-
# _your_login_ - Поменять на ваш логин в панели управления
# _your_project_ - Поменять на имя вашего проекта
# _your_server_ - Поменять на имя вашего сервера (Указано на вкладке "FTP и SSH" вашей панели управления)
# set :repository - Установить расположение вашего репозитория
# У вас должна быть настроена авторизация ssh по сертификатам

set :application, "dapteka"
# set :repository,  "ssh://hosting_ssidelnikov@hydrogen.locum.ru/home/hosting_ssidelnikov/dapteka.git"
set :repo_url,  "git@bitbucket.org:ssidelnikov/dsl-server-git.git"
set :rvm_ruby_version, '2.1.5'
set :bundle_path, '~/.gem'

dpath = "/home/hosting_ssidelnikov/projects/dapteka"

set :user, "hosting_ssidelnikov"
set :use_sudo, false
set :deploy_to, dpath

set :linked_dirs, %w{bin log tmp vendor/bundle public/system public/sitemaps}
set :linked_files, %w{config/database.yml config/settings.local.yml}

set :scm, :git

# role :web, "hydrogen.locum.ru", user: 'hosting_ssidelnikov'                          # Your HTTP server, Apache/etc
# role :app, "hydrogen.locum.ru", user: 'hosting_ssidelnikov'                          # This may be the same as your `Web` server
# role :db,  "hydrogen.locum.ru", user: 'hosting_ssidelnikov', :primary => true # This is where Rails migrations will run

set :unicorn_rails, "/var/lib/gems/1.8/bin/unicorn_rails"
set :unicorn_conf, "/etc/unicorn/dapteka.ssidelnikov.rb"
set :unicorn_pid, "/var/run/unicorn/dapteka.ssidelnikov.pid"

